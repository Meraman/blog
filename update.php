<?php 
	include('conn.php');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Blog</title>
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.1/tinymce.min.js"></script>
	<script src="./js/setdata.js"></script>
</head>
<body>
	<?php
		if(isset($_GET['id']))
		{
			$id=$_GET['id'];
			$sql="SELECT * FROM blogtable WHERE id='$id'";
			$result=$conn->query($sql);
			
			while($row=$result->fetch_assoc())
			{
				$name=$row['name'];
				$description=$row['description'];
				
	?>
	<form action="#" method="post" enctype="multipart/form-data">
		<div class="container">
	  		<div class="row">
	    		<div class="col-sm-12">
					<div class="jumbotron text-center">
				  		<h2>Update Post</h2>
					</div>
					<div class="form-group">
						<label for="usr">Name:</label>
						<input type="text" name="name" placeholder="Enter name" value="<?php echo $name; ?>" class="form-control" >
					</div>
					<div class="form-group">
						<label for="comment">Description:</label>
						<textarea name="description"  required="" ><?php echo $description; ?></textarea>
					</div>
					<div class="form-group">
						<label for="usr">Images:</label>
						<input type="file" name="image" class="form-control" >
					</div>
					<div class="form-group">
						<input type="submit" name="submit" class="btn btn-success btn-block"  value="Update">
					</div>
				</div>
			</div>
		</div>
	</form>
<?php } } ?>
</body>
</html>


<?php
	include('conn.php');
	if(isset($_GET['id']))
	{
  		$id=$_GET['id'];
 		if(isset($_POST['submit']))
  		{
			$name=$_POST['name'];
		    $description=$_POST['description'];
		    
		    $uploaddir = 'C:/xampp/htdocs/meraman/blog/uploads/';
		    $uploadfile = $uploaddir . basename($_FILES['image']['name']);
		    $image=$_FILES['image']['name'];
		    if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) 
		    {
		        
		        $sql="UPDATE blogtable SET 
				name='$name', description='$description', image='$image' WHERE id='$id'";
		  		// echo $sql;
		        if ($conn->query($sql)===True) {
		            echo "successfully";
		            header("Location:index.php");
		            
		        } 
		        else{
		            echo "Error: " . $sql . "<br>" . $conn->error;
		        }

		    } 
		    else {
	        	echo "Upload failed";
	    	}
	    }
	}	
?>