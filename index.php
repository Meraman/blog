<!DOCTYPE html>
<html>
<head>
  <title></title>

<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body>
  <div class="container">
    <div id="blog" class="row"> 
      <div class="jumbotron text-center">
        <h2>Blog</h2>
      </div>
        <ul class="nav">
          <li class="nav-item">
            <a href="adddata.html" class="nav ">Add New Blog</a>   
          </li>
        </ul>       
      </div>    
          <?php
            include("conn.php");
            $quer=mysqli_query($conn,"select * from blogtable");
            while($res=mysqli_fetch_array($quer))
            {
              $id=$res['id'];
            ?>
          <div class="col-md-10 blogShort">
            <h1><?= $res['name'] ?></h1>
                <?php
                  echo '<img height="100px" class="pull-left img-responsive thumb margin10 img-thumbnail" width="100px"  src="uploads/'. $res['image'] .'"/>';
                ?> 
                <article><p><?= $res['description']?></p></article>
                <a href="delete.php?id=<?php echo $id; ?>" class="btn btn-danger pull-right" onclick="return confirm('Are you sure you wish to delete this Record?');">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    <span><strong>Delete</strong></span>            
                </a>
                <a href="update.php?id=<?php echo $id; ?>" class="btn btn-primary pull-right ">
                  <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                  <span><strong>Edit</strong></span>            
                </a>
                  
                 
          </div>
          <?php } ?>
        <div class="col-md-12 gap10"></div>
    </div>
</div>
</body>
</html>